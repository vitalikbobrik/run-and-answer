using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using StatePatternInUnity;
public class CollectableBrain : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.transform.TryGetComponent(out PlayerController player))
        {
            transform.GetComponent<Animator>().SetTrigger("Run");
            enabled = false;
            transform.SetParent(player.transform);
        }
    }
}
