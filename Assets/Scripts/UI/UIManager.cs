using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;
public class UIManager : MonoBehaviour
{
    [SerializeField] private Button _startBtn;
    [SerializeField] private GameObject _announcer;
    [SerializeField] private RectTransform _pinRect;
    [SerializeField] private Transform _smallBrain;
    [SerializeField] private Transform _bigBrain;
    [SerializeField] private Transform _bonusWheel;

    [SerializeField] private RectTransform _iqLevel;
    [SerializeField] private Image _iqFill;
    [SerializeField] private TextMeshProUGUI _iqText;


    private int _iqTextInt = 0;

    private void OnEnable()
    {
        _startBtn.onClick.AddListener(() =>
        {
            GameAnalyticsSDK.GameAnalytics.NewProgressionEvent(GameAnalyticsSDK.GAProgressionStatus.Start, ActivityManager.GetPlayerLevel().ToString());

            _startBtn.gameObject.SetActive(false);
            EventHandler.GameStarted();
        });
        EventHandler.OnBrainIncrease += UpdatePin;
        EventHandler.OnRightAnswer += ShowAnnouncer;
        EventHandler.OnLevelCompleted += ShowBonusWheel;
        EventHandler.OnGameStarted += ShowIQLevel;
    }

    private void ShowIQLevel()
    {
        _iqLevel.DOAnchorPosY(0, 1);
    }

    private void OnDisable()
    {
        _startBtn.onClick.RemoveAllListeners();
        EventHandler.OnBrainIncrease -= UpdatePin;
        EventHandler.OnRightAnswer -= ShowAnnouncer;
        EventHandler.OnLevelCompleted -= ShowBonusWheel;
    }

    private void ShowBonusWheel()
    {
        _bonusWheel.gameObject.SetActive(true);
    }

    private void ShowAnnouncer()
    {
        _announcer.SetActive(true);
    }

    private void UpdatePin(bool update)
    {
        if (update)
        {
            _iqFill.DOFillAmount(_iqFill.fillAmount + 0.05f, 0.1f);
            _iqTextInt += 10;
        }

        else
        {
            _iqFill.DOFillAmount(_iqFill.fillAmount - 0.05f, 0.1f);
            _iqTextInt -= 5;
        }
        _iqText.text = "IQ " + _iqTextInt.ToString();
    }
}
