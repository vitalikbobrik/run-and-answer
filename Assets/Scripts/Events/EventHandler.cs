using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public static class EventHandler
{
    public delegate void GameEvents();
    public static GameEvents OnGameStarted;
    public static void GameStarted()
    {
        OnGameStarted?.Invoke();
    }

    public static GameEvents OnLevelCompleted;
    public static void LevelCompleted()
    {
        OnLevelCompleted?.Invoke();
    }

    public static GameEvents OnRightAnswer;
    public static void RightAnswer()
    {
        OnRightAnswer?.Invoke();
    }

    public delegate void GameEventsBool(bool param);

    public static GameEventsBool OnBrainIncrease;
    public static void BrainIncrease(bool increase)
    {
        OnBrainIncrease?.Invoke(increase);
    }
}
