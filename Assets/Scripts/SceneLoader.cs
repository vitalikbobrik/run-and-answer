using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class SceneLoader : MonoBehaviour
{
    private float _levelTime;

    private void Start()
    {
        _levelTime = Time.time;
    }

    public void ReloadScene()
    {
        SceneManager.LoadScene(0);
    }

    public void LoadNextLevel()
    {
        if(SceneManager.GetActiveScene().buildIndex == 9)
        {
            SceneManager.LoadScene(4);
        }
        else
        {
            GameAnalyticsSDK.GameAnalytics.NewProgressionEvent(GameAnalyticsSDK.GAProgressionStatus.Complete, ActivityManager.GetPlayerLevel().ToString(), (int)(Time.time - _levelTime));

            ActivityManager.UpdatePlayerLevel();
            if (ActivityManager.GetPlayerLevel() == 4)
            {
                SceneManager.LoadScene(9);
                ActivityManager.UnlockSkin();
            }
            else
            {
                if(SceneManager.GetActiveScene().buildIndex == 8)
                {
                    SceneManager.LoadScene(1);
                }
                else
                {
                    SceneManager.LoadScene((SceneManager.GetActiveScene().buildIndex + 1) % SceneManager.sceneCountInBuildSettings);
                }
            }

        }
    }
}
