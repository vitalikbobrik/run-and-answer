using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Cinemachine;
public class CameraController : MonoBehaviour
{
    public CameraSettings settings;
    private CinemachineVirtualCamera cinemachine;

    private void Start()
    {
        cinemachine = GetComponent<CinemachineVirtualCamera>();
    }

    public void SetCameraPosition(CameraTransfrom camera)
    {
        
        transform.DORotate(camera.CameraRotation, 0.5f);
        DOTween.To(() => cinemachine.GetCinemachineComponent<CinemachineFramingTransposer>().m_CameraDistance,
            x => cinemachine.GetCinemachineComponent<CinemachineFramingTransposer>().m_CameraDistance = x,
            camera.CameraDistance, 0.5f);
        DOTween.To(() => cinemachine.GetCinemachineComponent<CinemachineFramingTransposer>().m_TrackedObjectOffset,
            x => cinemachine.GetCinemachineComponent<CinemachineFramingTransposer>().m_TrackedObjectOffset = x,
            camera.ObjectOffset, 0.5f);
        DOTween.To(() => cinemachine.GetCinemachineComponent<CinemachineFramingTransposer>().m_ScreenY,
                x => cinemachine.GetCinemachineComponent<CinemachineFramingTransposer>().m_ScreenY = x,
            camera.ScreenX, 0.5f);

    }
}
