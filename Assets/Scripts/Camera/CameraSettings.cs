using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "Camera", menuName = "ScriptableObjects/CameraSettings")]
public class CameraSettings : ScriptableObject
{
    public CameraTransfrom Idle;
    public CameraTransfrom Run;
}
[System.Serializable]
public struct CameraTransfrom
{
    public Vector3 ObjectOffset;
    public Vector3 CameraRotation;
    public float CameraDistance;
    public float ScreenX;
}