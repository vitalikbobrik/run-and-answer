using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using StatePatternInUnity;
public class Collectable : MonoBehaviour
{
    [SerializeField] private ParticleSystem _starParticles;
    [SerializeField] private bool _isIncreasing = true;
    private void OnTriggerEnter(Collider other)
    {
        _starParticles.Play();
        Destroy(transform.GetChild(0).gameObject);
        if(other.TryGetComponent(out PlayerController player))
        {
            EventHandler.BrainIncrease(_isIncreasing);
        }
    }
}
