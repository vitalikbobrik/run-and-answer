using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using StatePatternInUnity;
public class TVLevel : MonoBehaviour
{
    private void OnEnable()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.AddComponent<BoxCollider>();
            transform.GetChild(i).gameObject.AddComponent<Rigidbody>();
            transform.GetChild(i).GetComponent<Rigidbody>().isKinematic = true;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Finish");
        if(other.TryGetComponent(out PlayerController player))
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                transform.GetChild(i).GetComponent<Rigidbody>().isKinematic = false;
                transform.GetChild(i).GetComponent<Rigidbody>().AddForce(new Vector3(Random.Range(0.5f,1), Random.Range(0.5f, 1) , Random.Range(0.5f, 1) )* 50, ForceMode.Impulse);
            }
        }
    }
}
