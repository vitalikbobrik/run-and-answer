using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using StatePatternInUnity;
using DG.Tweening;
using TMPro;
using UnityEngine.UI;
using Dreamteck.Splines;
public enum CorrectAnswerSide
{
    Left,
    Right
}

public class Answer : MonoBehaviour
{
    [SerializeField] private QuestionsSO _questionSo;

    [SerializeField] private TextMeshProUGUI _questionText;
    [SerializeField] private TextMeshProUGUI _leftAnswerText;
    [SerializeField] private SpriteRenderer _leftAnswer;
    [SerializeField] private SpriteRenderer _rightAnswer;
    [SerializeField] private TextMeshProUGUI _rightAnswerText;


    private CorrectAnswerSide _correctAnswerSide;

    //[SerializeField] private CorrectAnswerSide _correctAnswerSide;
    private BoxCollider _boxCollider;

    private void Start()
    {
        _boxCollider = GetComponent<BoxCollider>();
        _correctAnswerSide = _questionSo.side;
        _questionText.text = _questionSo.Question;
        if (_questionSo.LeftAnswer)
        {
            _leftAnswerText.transform.parent.gameObject.SetActive(false);
            _rightAnswerText.transform.parent.gameObject.SetActive(false);
            _leftAnswer.sprite = _questionSo.LeftAnswer;
            _rightAnswer.sprite = _questionSo.RightAnswer;
        }
        else
        {
            _leftAnswer.gameObject.SetActive(false);
            _rightAnswer.gameObject.SetActive(false);
            _leftAnswerText.text = _questionSo.LeftAnswerText;
            _rightAnswerText.text = _questionSo.RightAnswerText;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.TryGetComponent(out PlayerController player))
        {
            transform.DOMoveY(transform.position.y - 0.2f, 0.5f);
            transform.DORotate(transform.eulerAngles + new Vector3(90, 0, 0), 0.5f);
            _boxCollider.enabled = false;
            var follower = player.GetComponent<SplineFollower>().motion.offset;
            switch (_correctAnswerSide)
            {
                case CorrectAnswerSide.Left:
                    if (follower.x > 0)
                    {
                        player.FallState();
                        EventHandler.BrainIncrease(false);
                    }
                    else
                    {
                        EventHandler.RightAnswer();
                        EventHandler.BrainIncrease(true);

                    }
                    break;
                case CorrectAnswerSide.Right:
                    if (follower.x < 0)
                    {
                        player.FallState();
                        EventHandler.BrainIncrease(false);

                    }
                    else
                    {
                        EventHandler.RightAnswer();
                        EventHandler.BrainIncrease(true);
                    }
                    break;
            }
        }
    }
}
