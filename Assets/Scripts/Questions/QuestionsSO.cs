using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "Question", menuName = "ScriptableObjects/Questions", order = 1)]
public class QuestionsSO : ScriptableObject
{
    public string Question;
    public Sprite LeftAnswer;
    public Sprite RightAnswer;
    public CorrectAnswerSide side;
    public string LeftAnswerText;
    public string RightAnswerText;
}
