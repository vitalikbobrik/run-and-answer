using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Lean.Touch;
namespace StatePatternInUnity
{
    public class RunState : State
    {
        private bool _allowRun = false;
        public RunState(PlayerController player, StateMachine stateMachine) : base(player, stateMachine)
        {
        }

        public override void Enter()
        {
            base.Enter();
            player.PlayerAnimator.SetTrigger("Run");
            player.SplineFollower.followSpeed = player.PlayerSpeed;
        }

        public override void LogicUpdate()
        {
            base.LogicUpdate();
            if(LeanTouch.Fingers.Count > 0 && Input.GetMouseButton(0))
            {
                player.SplineFollower.motion.offset += new Vector2(Lean.Touch.LeanTouch.Fingers[0].ScreenDelta.x/100,0);
            }
            player.SplineFollower.motion.offset = new Vector2(Mathf.Clamp(player.SplineFollower.motion.offset.x, -3f, 3f), 0);
        }

        public override void Exit()
        {
            base.Exit();
            player.SplineFollower.followSpeed = 0;
        }
    }
}

