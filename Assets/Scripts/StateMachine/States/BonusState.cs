using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Cinemachine;
namespace StatePatternInUnity
{
    public class BonusState : State
    {
        public BonusState(PlayerController player, StateMachine stateMachine) : base(player, stateMachine)
        {
        }

        public override void Enter()
        {
            base.Enter();

            player.SplineFollower.enabled = false;
            player.transform.DOJump(player.WeightPos.position, 3, 5, 4).SetEase(Ease.Linear).OnComplete(() =>
            {
                EventHandler.LevelCompleted();
                player.gameObject.SetActive(false);
                DOTween.To(() => player.CinemachineVirtualCamera.GetCinemachineComponent<Cinemachine3rdPersonFollow>().ShoulderOffset,
                    x => player.CinemachineVirtualCamera.GetCinemachineComponent<Cinemachine3rdPersonFollow>().ShoulderOffset = x, new Vector3(0, 2.5f, 0), 1);
                DOTween.To(() => player.CinemachineVirtualCamera.GetCinemachineComponent<Cinemachine3rdPersonFollow>().CameraDistance,
                    x => player.CinemachineVirtualCamera.GetCinemachineComponent<Cinemachine3rdPersonFollow>().CameraDistance = x, 5.5f, 1).OnComplete(() =>
                    {
                        player.ConfettiParticles.Play();
                    });

            });
            player.StartCoroutine(player.DeselectAll());
        }

    }
}

