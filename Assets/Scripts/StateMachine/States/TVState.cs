using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Cinemachine;
namespace StatePatternInUnity
{
    public class TVState : State
    {
        public TVState(PlayerController player, StateMachine stateMachine) : base(player, stateMachine)
        {
        }

        public override void Enter()
        {
            base.Enter();
            player.SplineFollower.enabled = false;

            Debug.Log("Enter");
            player.transform.DOMoveZ(245, 3).OnComplete(() =>
            {
                player.PlayerAnimator.SetTrigger("Idle");
                EventHandler.LevelCompleted();
                player.ConfettiParticles.Play();
                DOTween.To(() => player.CinemachineVirtualCamera.GetCinemachineComponent<Cinemachine3rdPersonFollow>().ShoulderOffset,
                    x => player.CinemachineVirtualCamera.GetCinemachineComponent<Cinemachine3rdPersonFollow>().ShoulderOffset = x, new Vector3(5, 7f, 0), 1);
                DOTween.To(() => player.CinemachineVirtualCamera.GetCinemachineComponent<Cinemachine3rdPersonFollow>().CameraDistance,
                    x => player.CinemachineVirtualCamera.GetCinemachineComponent<Cinemachine3rdPersonFollow>().CameraDistance = x, -20f, 1);

            });

        }
    }
}

