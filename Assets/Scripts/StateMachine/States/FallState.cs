using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
namespace StatePatternInUnity
{
    public class FallState : State
    {
        public FallState(PlayerController player, StateMachine stateMachine) : base(player, stateMachine)
        {
        }

        public override void Enter()
        {
            base.Enter();
            player.PlayerAnimator.SetTrigger("FallBack");
            DOTween.Sequence().AppendInterval(0.7f).AppendCallback(() =>
            {
                player.playerSM.ChangeState(player.run);
            });
        }
    }
}

