﻿

using UnityEngine;

namespace StatePatternInUnity
{
    public class IdleState : State
    {
        public IdleState(PlayerController player, StateMachine stateMachine) : base(player, stateMachine)
        {
        }

        public override void Enter()
        {
            base.Enter();
            //player.CameraController.SetCameraPosition(player.CameraController.settings.Idle);
        }

        public override void Exit()
        {
            base.Exit();
        }

        public override void HandleInput()
        {
            base.HandleInput();

        }

        public override void PhysicsUpdate()
        {
            base.PhysicsUpdate();
        }
    }
}