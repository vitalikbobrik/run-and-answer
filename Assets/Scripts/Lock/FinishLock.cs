using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean.Touch;
public class FinishLock : MonoBehaviour
{
    [SerializeField] private int[] _lockAnswer;
    private LeanMultiUpdate leanMultiUpdate;
    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit))
            {
                if(hit.transform.TryGetComponent(out LeanMultiUpdate lean))
                {
                    leanMultiUpdate = lean;
                    lean.enabled = true;
                }
                //Transform objectHit = hit.transform;

                // Do something with the object that was hit by the raycast.
            }

        }
        if (Input.GetMouseButtonUp(0))
        {
            if (leanMultiUpdate != null)
            {
                Debug.Log(GetNumber(leanMultiUpdate.transform));
                leanMultiUpdate.enabled = false;
                leanMultiUpdate = null;
            }
        }
    }

    private int GetNumber(Transform locker)
    {
        Debug.Log(locker.transform.eulerAngles.x);
        if(locker.transform.localEulerAngles.x > 0)
        {
            return 10 - Mathf.RoundToInt(locker.transform.localEulerAngles.x / 36);
        }
        else
        {
            return -Mathf.RoundToInt(locker.transform.localEulerAngles.x / 36);
        }
        
    }
}
