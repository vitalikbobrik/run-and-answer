using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using StatePatternInUnity;
using DG.Tweening;
[RequireComponent(typeof(BoxCollider))]
public class Obstacle : MonoBehaviour
{
    private BoxCollider _collider;

    private void Start()
    {
        _collider = GetComponent<BoxCollider>();
    }
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.transform.TryGetComponent(out PlayerController player))
        {
            player.playerSM.ChangeState(player.fall);
            _collider.enabled = false;
        }
    }
}
