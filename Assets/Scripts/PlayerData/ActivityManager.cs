using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ActivityManager
{
    public static int GetPlayCount() => PlayerPrefs.GetInt("count", 1);
    public static void UpdatePlayCount() => PlayerPrefs.SetInt("count", GetPlayCount() + 1);

    public static int GetPlayerLevel() => PlayerPrefs.GetInt("level", 1);
    public static void UpdatePlayerLevel() => PlayerPrefs.SetInt("level", GetPlayerLevel() + 1);

    public static bool SkinUnlocked() => PlayerPrefs.GetInt("skin") == 2;
    public static void UnlockSkin() => PlayerPrefs.SetInt("skin", 2);
}
