using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using Dreamteck.Splines;
using Lean.Touch;
using DG.Tweening;
namespace StatePatternInUnity
{
    public enum FinishType
    {
        Jump,
        TV
    }

    public class PlayerController : MonoBehaviour
    {
        public StateMachine playerSM;
        public IdleState idle;
        public RunState run;
        public FallState fall;
        public BonusState bonus;
        public TVState tv;

        [SerializeField] private ParticleSystem _right;
        [SerializeField] private ParticleSystem _wrong;

        [SerializeField] private Material _unlockedMat;

        [SerializeField] private FinishType finishType;
        public FinishType FinishType => finishType;

        [SerializeField] private GameObject bonusLevel;
        [SerializeField] private GameObject tvLevel;


        private Rigidbody _playerRb;
        public Rigidbody PlayerRB => _playerRb;
        [SerializeField] private GameObject[] _characters;
        public GameObject[] Characters => _characters;

        [SerializeField] private ParticleSystem _boomParticles;

        [SerializeField] private GameObject _nextBtn;

        private SplineFollower _follower;
        public SplineFollower SplineFollower => _follower;

        [SerializeField] private Transform _headBone;
        public Transform HeadBone => _headBone;

        [SerializeField] private Transform _weightPos;
        public Transform WeightPos => _weightPos;

        [SerializeField] private ParticleSystem _confettiParticles;
        public ParticleSystem ConfettiParticles => _confettiParticles;
        
        [SerializeField] private float _playerSpeed;
        public float PlayerSpeed => _playerSpeed;

        [SerializeField] private CinemachineVirtualCamera cinemachine;
        public CinemachineVirtualCamera CinemachineVirtualCamera => cinemachine;

        private Animator _playerAnimator;
        public Animator PlayerAnimator => _playerAnimator;

        private void OnEnable()
        {
            EventHandler.OnGameStarted += SwitchToRunState;
            EventHandler.OnBrainIncrease += IncreaseBrainSize;
            EventHandler.OnRightAnswer += EventHandler_OnRightAnswer;
            if (ActivityManager.SkinUnlocked())
            {
                transform.GetChild(0).GetChild(1).GetComponent<Renderer>().material = _unlockedMat;
            }
        }

        private void EventHandler_OnRightAnswer()
        {
            _right.Play();
        }

        private void _follower_onEndReached(double obj)
        {
            switch (finishType)
            {
                case FinishType.Jump:
                    playerSM.ChangeState(bonus);
                    break;
                case FinishType.TV:
                    playerSM.ChangeState(tv);
                    break;
            }
            
        }

        private void OnDisable()
        {
            EventHandler.OnGameStarted -= SwitchToRunState;
            EventHandler.OnBrainIncrease -= IncreaseBrainSize;
            EventHandler.OnRightAnswer -= EventHandler_OnRightAnswer;
        }

        private void SwitchToRunState()
        {
            playerSM.ChangeState(run);
        }

        public void FallState()
        {
            _wrong.Play();
            playerSM.ChangeState(fall);
        }

        private void IncreaseBrainSize(bool increase)
        {
            if (increase)
            {
                transform.DOScale(transform.localScale * 1.05f, 0.3f);
            }
            else
            {
                transform.DOScale(transform.localScale * 0.95f, 0.3f);
            }
        }

        public IEnumerator DeselectAll()
        {
            foreach (GameObject unit in Characters)
            {
                yield return new WaitForSeconds(0.5f);
                Instantiate(_boomParticles, unit.transform.position, Quaternion.identity);
                yield return new WaitForSeconds(0.5f);

                unit.SetActive(false);
            }
        }

        private void Start()
        {

            switch (finishType)
            {
                case FinishType.Jump:
                    bonusLevel.SetActive(true);
                    break;
                case FinishType.TV:
                    tvLevel.SetActive(true);
                    break;
            }

            playerSM = new StateMachine();
            idle = new IdleState(this, playerSM);
            run = new RunState(this, playerSM);
            fall = new FallState(this, playerSM);
            bonus = new BonusState(this, playerSM);
            tv = new TVState(this, playerSM);
            playerSM.Initialize(idle);

            _follower = GetComponent<SplineFollower>();
            //_follower.onEndReached += _follower_onEndReached;
            _playerRb = GetComponent<Rigidbody>();
            _playerAnimator = transform.GetChild(0).GetComponent<Animator>();

            _follower.onEndReached += _follower_onEndReached;

        }





        private void Update()
        {
            playerSM.CurrentState.LogicUpdate();
            playerSM.CurrentState.HandleInput();
        }
        private void FixedUpdate()
        {
            playerSM.CurrentState.PhysicsUpdate();
        }
        public void AddForce()
        {

            DOTween.To(() => _follower.motion.offset,
                    x => _follower.motion.offset = x,
                    new Vector2(_follower.motion.offset.x, 5), 0.5f).SetLoops(2, LoopType.Yoyo);
        }
    }
}

