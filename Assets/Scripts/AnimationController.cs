using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class AnimationController
{ 
    #region PUSHUP
    public static readonly string START_PUSHUP = "StartPushUp";
    public static readonly string TRY_PUSHUP = "TryPushUp";
    public static readonly string STOP_PUSHUP = "StopPushUp";
    #endregion


}
